# betapathogen

Data and code to reproduce the results in [Martínez Minaya et al. 
(2019)](https://www.biorxiv.org/content/10.1101/771667v1.article-info) [1], which models Inoculum Availability of ***Plurivorosphaerella nawae*** in Persimmon Leaf Litter with Bayesian Beta Regression using [INLA](http://www.r-inla.org/).

This package contains one vignette [vignettes/](vignettes) with all the code necessary to reproduce the results from the paper. Data can be loaded once the package has been installed or can be checked in the folder [data_raw](data_raw).

## Previous request
Previously to install the ***betapathogen*** package, it is required to install ***INLA*** package from an external repository:
[http://www.r-inla.org/download](http://www.r-inla.org/download)

## Installing
Once you have it, you can use this repository in two different ways:

**Installation cloning the folder**

  You can peek at the code online here or you can clone the repository. The code for fitting the models and producing all results is within [vignettes/](vignettes). You can install locally the package setting the working directory to the folder which contains the package ***betapathogen*** and using the next commands.

```r
if(!require(devtools)) install.packages('devtools')
install_local("betapathogen")
```

**Installation from R directly**

You can install directly from **R** using the following code:

``` r 
if(!require(devtools)) install.packages('devtools')
devtools:::install_bitbucket("joaquin-martinez-minaya/betapathogen", build_vignettes = TRUE)
```

## Important information
When the operative system is Windows, the following error message is displayed in some cases:

*Error: Failed to install 'betapathogen' from Bitbucket: JSON: EXPECTED value GOT*

Usually it is solved updating the R version. But it is not always a solution. As it is not a problem of this package, it is more a general problem from R, we recommend to install the package using the local way (Installation cloning the folder).

## How to use it
  As the main code which generates the model is in vignettes, you can use the next commands to see it.
    
``` r
library(betapathogen)
browseVignettes('betapathogen')  # vignettes
```
Also, you can check the information of the datasets and use them.
    
``` r
?AlcudiaClim
```


[1] **J. Martínez-Minaya, D. Conesa, A. López-Quílez, J. Luis Mira and A. Vicent**. Modelling Inoculum Availability of **Plurivorosphaerella nawae** in Persimmon Leaf Litter with Bayesian Beta Regression. [bioRXiv](https://www.biorxiv.org/content/10.1101/771667v1.article-metrics)

