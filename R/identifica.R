
#######################################################
########## Funci?n identifica #########################
########## Identifica donde se han producido esos saltos#
#########################################################
identifica<-function(fechas)
{
  fechas<-as.numeric(fechas)
  diferencia<-fechas[2]-fechas[1]
  for (i in 2:(length(fechas)-1))
  {
    diferencia<-c(diferencia, fechas[i+1]-fechas[i])
  }
  diferencia
}