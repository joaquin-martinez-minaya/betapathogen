#' Computing degree days between two dates
#'
#' `DD.acum.fecha` computes degree days between two dates. Three options are available: \emph{dd}, which represents degree days; \emph{ddvpd}, which represents the degree days taking into account the
#' vapour pressure deficit as computed in Rossi et al. (2009); and \emph{ddwet}, which are the degree days taking into account the vapour pressure deficit
#' and the rainfall as computed in Rossi et al. (2009).
#'
#' Rossi, V., Salinari, F., Pattori, E., Giosue, S., & Bugiani, R. (2009). Predicting the dynamics of ascospore maturation of Venturia pirina based on environmental factors. Phytopathology, 99(4), 453-461.
#'
#' @param Finic: initial date to accumulate.
#' @param Ffinal: final date to stop accumulating.
#' @param base: base temperature to accumulate.
#' @param datos.diarios: daily data to accumulate. It should contain climatic variables of the interested locations. It has to contain four columns
#' Fecha (date in date format), Tempmedia (mean temperature), HRmedia (relative humidity (\%)) and Prec (precipitation mm)
#' @param dd: degree days already accumulated.
#' @param variable: three options are available: \emph{dd}, which represents degree days; \emph{ddvpd}, which represents the degree days taking into account the
#' vapour pressure deficit as computed in Rossi et al. (2009); and \emph{ddwet}, which are the degree days taking into account the vapour pressure deficit
#' and the rainfall as computed in Rossi et al. (2009).
#'
#' @return a value with the dd, ddvpd or ddwet, depending on the parameter variable.
#' @examples
#' alcudia.temp <- AlcudiaClim
#' alcudia.temp <- change_format(datos.temp = alcudia.temp,
#'                               loc        = "alcudia")
#'
#' DD.acum.fecha(Finic         = "2010-05-01",
#'               Ffinal        = "2010-08-01",
#'               base          = 0,
#'               datos.diarios = alcudia.temp,
#'               dd            = 0,
#'               variable      = "dd")
#'
#' DD.acum.fecha(Finic         = "2010-05-01",
#'               Ffinal        = "2010-08-01",
#'               base          = 0,
#'               datos.diarios = alcudia.temp,
#'               dd            = 0,
#'               variable      = "ddvpd")
#'
#' DD.acum.fecha(Finic         = "2010-05-01",
#'               Ffinal        = "2010-08-01",
#'               base          = 0,
#'               datos.diarios = alcudia.temp,
#'               dd            = 0,
#'               variable      = "dd")
#'
#'
#' @author Joaquín Martínez-Minaya <\email{jomarminaya@@gmail.com}>
DD.acum.fecha<-function( Finic, Ffinal, base, datos.diarios, dd=0, variable = "dd")
{

  if (!(variable == 'dd' | variable == 'ddvpd' | variable == 'ddwet'))
  {
    stop("The variable only takes the values 'dd', 'ddvpd' or 'ddwet'")
  }
  #Calculamos los d?as que hay entre las dos fechas
  tam <- length(seq(as.Date(Finic), as.Date(Ffinal), by="day"))

  #La primera condici?n es que los datos est?n en ese rango
  cond1 <- datos.diarios[, 1]>(as.Date(Finic)) & (datos.diarios[, 1]<=as.Date(Ffinal))

  #La segunda condici?n es que los grados d?a que acumulemos sean mayores que la base
  cond2 <- datos.diarios[, 2]>=base

  cond_total <- cond1 & cond2

  if(variable == 'ddvpd' | variable == 'ddwet')
  {
    #Introducimos una nueva condici?n: (vpd)i ???  4 hPa,
    vpd<-(1-datos.diarios[,3]/100)*  6.11 * exp((17.47 * datos.diarios[,2])/(239 + datos.diarios[,2]))
    #vpd<-(1-datos.diarios[,3]/100) * 610.7*10^(7.5 * datos.diarios[,2]/(237.3+datos.diarios[,2]))
    #vpd<-(1-datos.diarios[[1]][,3]/100)*  6.11 * exp((17.47 * datos.diarios[[1]][,2])/(239 + datos.diarios[[1]][,2]))

    cond3<-(vpd <= 4)
    cond_total <- cond_total & cond3
  }

  if(variable == 'ddwet')
  {
    cond4 <- datos.diarios[,4] >= 0.2
    cond_total <- cond_total & cond4
  }

  #Calculamos los grados d?a ah?
  dd <- dd + sum(datos.diarios[,2][cond_total] - base) #Grados d?a acumulados

  dd
}



